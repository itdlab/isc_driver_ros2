cmake_minimum_required(VERSION 3.5.0)

project(isc_driver)

# Default to C++14
if(NOT CMAKE_CXX_STANDARD)
    set(CMAKE_CXX_STANDARD 14)
endif()

# Model select. 1 if VM model, 2 if XC model
set(ISC_MODEL 2)
if( ${ISC_MODEL} EQUAL 1)
set(TARGET_NODE vm_driver_node)
else()
set(TARGET_NODE xc_driver_node)
endif()

set(iscsdk_INCLUDE_DIRS /usr/local/include/iscsdk${ISC_MODEL})
set(iscsdk_LIBRARIES /usr/local/lib/iscsdk${ISC_MODEL}/libISCLib.so /usr/local/lib/iscsdk${ISC_MODEL}/libISCSDKLib.so)

if(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_CXX_COMPILER_ID MATCHES "Clang")
		add_compile_options(-Wall -Wextra -Wpedantic -Wsign-compare)
endif()
add_compile_options(-D MODEL=${ISC_MODEL})

find_package(ament_cmake REQUIRED)
find_package(rclcpp REQUIRED)
find_package(rclcpp_components REQUIRED)
find_package(rclcpp_lifecycle REQUIRED)
find_package(lifecycle_msgs REQUIRED)
find_package(std_msgs REQUIRED)
find_package(sensor_msgs REQUIRED)
find_package(cv_bridge REQUIRED)
find_package(OpenCV 3 REQUIRED)
find_package(PCL COMPONENTS common REQUIRED)


include_directories(include
  ${cv_bridge_INCLUDE_DIRS}
  ${std_msgs_INCLUDE_DIRS}
  ${lifecycle_msgs_INCLUDE_DIRS}
  ${rclcpp_lifecycle_INCLUDE_DIRS}
  ${rclcpp_INCLUDE_DIRS}
  ${iscsdk_INCLUDE_DIRS}
  ${PCL_INCLUDE_DIRS}
  )


add_library(isc_driver SHARED
  src/isc_driver.cpp 
  src/isc_wrapper.cpp)
target_compile_definitions(isc_driver
  PRIVATE "COMPOSITION_BUILDING_DLL")

ament_target_dependencies(isc_driver
  "pcl_conversions"
  "rcl_interfaces"
  "rclcpp"
  "rclcpp_lifecycle"
  "lifecycle_msgs"
  "sensor_msgs"
  "std_msgs"
  "cv_bridge"
  "OpenCV")

rclcpp_components_register_nodes(isc_driver "isc_driver::ISCDriver")
set(node_plugins "${node_plugins}isc_driver::ISCDriver;$<TARGET_FILE:isc_driver>\n")

add_executable(${TARGET_NODE}
  src/isc_driver_node.cpp)

target_link_libraries(${TARGET_NODE}
  isc_driver
  ${iscsdk_LIBRARIES}
  ${PCL_LIBRARIES}
  )
ament_target_dependencies(${TARGET_NODE}
  "rclcpp"
  "sensor_msgs"
  "std_msgs"
  "cv_bridge"
  "OpenCV")


######################
install(TARGETS
isc_driver
ARCHIVE DESTINATION lib
LIBRARY DESTINATION lib
RUNTIME DESTINATION bin)


install(TARGETS
  ${TARGET_NODE}
  DESTINATION lib/${PROJECT_NAME})


#Install launch files.
install(DIRECTORY
 launch
 DESTINATION share/${PROJECT_NAME}
)

#Install param files.
install(DIRECTORY
 param
 DESTINATION share/${PROJECT_NAME}
)

ament_package()
