import os
import sys
from launch import LaunchDescription
from launch_ros.actions import LifecycleNode
from launch_ros.actions import Node

def generate_launch_description():
    return LaunchDescription([
        LifecycleNode(package='isc_driver', node_executable='xc_driver_node',
                      node_name='xc_driver_node', output='screen',
                      parameters=["xc_driver_node.yaml"])])

def main(argv=sys.argv[1:]):
    print('Main')
    generate_launch_description()

if __name__ == '__main__':
    print('__name__')
    main()

