//   Copyright 2019 ITDLab corporation
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "isc_driver/isc_driver.hpp"

#include <string>
#include <rclcpp/rclcpp.hpp>

int main(int argc, char * argv[])
{

  // Force flush of the stdout buffer.
  // This ensures a correct sync of all prints
  // even when executed simultaneously within a launch file.
  setvbuf(stdout, NULL, _IONBF, BUFSIZ);

  rclcpp::init(argc, argv);
  rclcpp::executors::SingleThreadedExecutor exec;

#if (MODEL==1)
  std::string model = "VM";
  std::string node_name = "vm_driver_node";
#else
  std::string model = "XC";
  std::string node_name = "xc_driver_node";
#endif
  auto isc = std::make_shared<isc_camera::ISCDriver>(model, node_name);

  exec.add_node(isc->get_node_base_interface());

  exec.spin();

  rclcpp::shutdown();

  return 0;
}
