//   Copyright 2019 ITDLab corporation
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

/**
 * @file    isc_driver.cpp
 * @brief	  Driver of ISC camera
 * @date	  2019/10/08
 * @author	Katsuichi Kigasawa
 */

#include "isc_driver/isc_driver.hpp"

#include <chrono>
#include <cstdio>
#include <memory>
#include <string>

#include <sensor_msgs/point_cloud2_iterator.hpp>
#include <sensor_msgs/msg/point_field.hpp>

#define COLOR_PALETTE_SIZE      256
#define STOP_WATCH  0

using namespace std::chrono_literals;

namespace isc_camera
{
ISCDriver::ISCDriver(const std::string model, const std::string & node_name, bool intra_process_comms)
: rclcpp_lifecycle::LifecycleNode(node_name,
    rclcpp::NodeOptions().use_intra_process_comms(intra_process_comms)),
  ISCWrapper()
{

  // ISC initialize
  img_buffer_ = (uint8_t *)malloc(MAX_IMAGE_SIZE * 3);
  right_buffer_ = (uint8_t *)malloc(MAX_IMAGE_SIZE * 3);
  pallarax_buffer_ = (float *)malloc(MAX_IMAGE_SIZE * sizeof(float));
  fps_ = 30;

  // ROS initialize
  frame_id_ = "isc_camera";

  model_ = model;
  width_ = 640;
  height_ = 480;
  center_x_ = width_ / 2;
  center_y_ = height_ / 2;
  is_start_ = false;

  frame_id_ = this->declare_parameter("frame_id", "isc_camera");
  fps_ = this->declare_parameter("fps", 30);
  skip_count_init_ = 60 / fps_;
  skip_count_ = skip_count_init_;

  is_publish_stereo_ = declare_parameter("publish.stereo", false);
  if (is_publish_stereo_ == false) {
    is_publish_raw_ = declare_parameter("publish.raw", true);
    is_publish_pallarax_ = this->declare_parameter("publish.pallarax", false);
    is_publish_cloud2_ = this->declare_parameter("publish.cloud2", true);
    is_publish_demo_ = this->declare_parameter("publish.demo", false);
  } else {
    is_publish_raw_ = false;
    is_publish_pallarax_ = false;
    is_publish_cloud2_ = false;
    is_publish_demo_ = false;
  }
  is_shutter_auto_ = this->declare_parameter("shutter.auto", true);
  shutter_gain_ = this->declare_parameter("shutter.gain", 16);
  shutter_exposure_ = this->declare_parameter("shutter.exposure", 479);

  if (model_.compare("VM") == 0) {
    width_ = 640;
    height_ = 480;
    center_x_ = width_ / 2;
    center_y_ = height_ / 2;
  } else {
    width_ = 1024;
    height_ = 720;
    center_x_ = width_ / 2;
    center_y_ = height_ / 2;
  }

  rclcpp_lifecycle::LifecycleNode::set_on_parameters_set_callback(
    std::bind(&isc_camera::ISCDriver::param_update_callback, this, std::placeholders::_1)
  );

  RCLCPP_INFO(this->get_logger(), "ISCDriver is started");
  RCLCPP_INFO(this->get_logger(), "   %s-model %s-frameid %dfps", model_.c_str(),
    frame_id_.c_str(), fps_);
  RCLCPP_INFO(
    this->get_logger(), "   Publishi stereo=%d raw=%d pallarax=%d cloud2=%d demo=%d", is_publish_stereo_, is_publish_raw_, is_publish_pallarax_, is_publish_cloud2_,
    is_publish_demo_);
  RCLCPP_INFO(
    this->get_logger(), "   Shutter auto=%d gain=%d exposure=%d", is_shutter_auto_, shutter_gain_,
    shutter_exposure_);
}

ISCDriver::~ISCDriver()
{
  free(img_buffer_);
  free(right_buffer_);
  free(pallarax_buffer_);
}


rcl_interfaces::msg::SetParametersResult
ISCDriver::param_update_callback(std::vector<rclcpp::Parameter> params)
{
      auto results = std::make_shared<rcl_interfaces::msg::SetParametersResult>();
      bool is_change = false;
      for(auto&& param : params){
        if(param.get_name() == "shutter.auto"){
          is_shutter_auto_ = param.as_bool();
          is_change = true;
          results->successful = true;
          results->reason = "";
        } else if(param.get_name() == "shutter.gain"){
          shutter_gain_ = param.as_int();
          is_change = true;
          results->successful = true;
          results->reason = "";
        } else if(param.get_name() == "shutter.exposure"){
          shutter_exposure_ = param.as_int();
          is_change = true;
          results->successful = true;
          results->reason = "";
        }else{
          results->successful = false;
          results->reason = "";
        }
      }
      if (is_change) {
        ISCWrapper::set_shutter_control(is_shutter_auto_, shutter_gain_, shutter_exposure_);
      }
      return *results;
    }

rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn
ISCDriver::on_configure(const rclcpp_lifecycle::State &)
{
  RCLCPP_INFO(this->get_logger(), "ISCDriver on_configure to INACTIVE");

//  rmw_qos_profile_t qos_profile = rmw_qos_profile_default;
  if (is_publish_stereo_ == false) {
    camera_info_pub_ = this->create_publisher<sensor_msgs::msg::Image>("/isc_camera/image_raw", 2);
    pallarax_info_pub_ =
      this->create_publisher<sensor_msgs::msg::Image>("/isc_camera/pallarax", 2);
    demo_info_pub_ = this->create_publisher<sensor_msgs::msg::Image>("/isc_camera/demo", 2);
    pcl_info_pub_ = this->create_publisher<sensor_msgs::msg::PointCloud2>("/isc_camera/point_cloud",
        2);
  } else {
    camera_info_pub_ = this->create_publisher<sensor_msgs::msg::Image>("/isc_camera/left/image_raw",
        2);
    stereo_right_info_pub_ = this->create_publisher<sensor_msgs::msg::Image>(
      "/isc_camera/right/image_raw", 2);
  }

  timer_ = this->create_wall_timer(
    1ms, std::bind(&ISCDriver::image_callback, this));

  return rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn::SUCCESS;
}

rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn
ISCDriver::on_activate(const rclcpp_lifecycle::State &)
{
  bool ret = this->start_grab();
  if (ret == true) {
    image_time_prev_ = std::chrono::system_clock::now();
    if (is_publish_stereo_) {
      camera_info_pub_->on_activate();
      stereo_right_info_pub_->on_activate();
    }
    if (is_publish_raw_) {
      camera_info_pub_->on_activate();
    }
    if (is_publish_pallarax_) {
      pallarax_info_pub_->on_activate();
    }
    if (is_publish_demo_) {
      demo_info_pub_->on_activate();
    }
    if (is_publish_cloud2_) {
      pcl_info_pub_->on_activate();
    }

    RCLCPP_INFO(this->get_logger(), "ISCDriver on_active SUCCESS to ACTIVE");
    return rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn::SUCCESS;
  } else {
    RCLCPP_ERROR(this->get_logger(), "ISCDriver on_active FAIL to FINALIZE");
    return rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn::FAILURE;
  }
}

rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn
ISCDriver::on_deactivate(const rclcpp_lifecycle::State &)
{
  RCLCPP_INFO(this->get_logger(), "ISCDriver on_deactive to INACTIVE");

  this->stop_grab();
  timer_->reset();
  if (is_publish_stereo_) {
    camera_info_pub_->on_deactivate();
    stereo_right_info_pub_->on_deactivate();
  }
  if (is_publish_raw_) {
    camera_info_pub_->on_deactivate();
  }
  if (is_publish_pallarax_) {
    pallarax_info_pub_->on_deactivate();
  }
  if (is_publish_demo_) {
    demo_info_pub_->on_deactivate();
  }
  if (is_publish_cloud2_) {
    pcl_info_pub_->on_deactivate();
  }
  return rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn::SUCCESS;
}

rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn
ISCDriver::on_cleanup(const rclcpp_lifecycle::State &)
{
  RCLCPP_INFO(this->get_logger(), "ISCDriver on_cleanup to UNCONFIGURE");
  this->stop_grab();
  timer_->reset();
  if (is_publish_stereo_) {
    camera_info_pub_->on_deactivate();
    stereo_right_info_pub_->on_deactivate();
  }
  if (is_publish_raw_) {
    camera_info_pub_->on_deactivate();
  }
  if (is_publish_pallarax_) {
    pallarax_info_pub_->on_deactivate();
  }
  if (is_publish_demo_) {
    demo_info_pub_->on_deactivate();
  }
  if (is_publish_cloud2_) {
    pcl_info_pub_->on_deactivate();
  }
  return rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn::SUCCESS;
}

rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn
ISCDriver::on_shutdown(const rclcpp_lifecycle::State &)
{
  RCLCPP_INFO(this->get_logger(), "ISCDriver on_shutdown to FINALIZE");
  this->stop_grab();
  timer_->reset();
  if (is_publish_stereo_) {
    camera_info_pub_->on_deactivate();
    stereo_right_info_pub_->on_deactivate();
  }
  if (is_publish_raw_) {
    camera_info_pub_->on_deactivate();
  }
  if (is_publish_pallarax_) {
    pallarax_info_pub_->on_deactivate();
  }
  if (is_publish_demo_) {
    demo_info_pub_->on_deactivate();
  }
  if (is_publish_cloud2_) {
    pcl_info_pub_->on_deactivate();
  }
  return rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn::SUCCESS;
}

bool ISCDriver::start_grab()
{
  bool ret = false;
  int error = 0;

  if ( (error=ISCWrapper::open()) == 0) {
    ISCWrapper::set_shutter_control(is_shutter_auto_, shutter_gain_, shutter_exposure_);
    ISCWrapper::get_size(&width_, &height_);
    ret = ISCWrapper::start_grab(is_publish_stereo_);
    if (ret == true) {
      is_start_ = true;
      mat_images_.release();
      mat_images_.create(height_ * 3, width_, CV_8UC1);
      right_mat_images_.release();
      right_mat_images_.create(height_ * 3, width_, CV_8UC1);
      mat_demo_images_.release();
      mat_demo_images_.create(height_, width_ * 2, CV_8UC3);
      make_color_table(color_table_);
    }
  }
  else {
    RCLCPP_ERROR(this->get_logger(), "ISCDriver camera open is failed %d", error);
  }
  return ret;
}

void ISCDriver::stop_grab()
{
  if (is_start_ == true) {
    is_start_ = false;
    ISCWrapper::stop_grab();
    ISCWrapper::close();
    mat_images_.release();
    right_mat_images_.release();
    mat_demo_images_.release();
  }
}

void ISCDriver::image_callback()
{
  if ( is_start_==true) {
    bool ret;
    if (is_publish_stereo_ == false) {
    /**
     * Publish Raw, pallarax, demo, PCL
     */
      ret = ISCWrapper::get_images(img_buffer_, pallarax_buffer_);
      if (ret == true) {
        skip_count_--;
        if( skip_count_ == 0 ) {
          skip_count_ = skip_count_init_;
#if (STOP_WATCH==1)
          std::chrono::system_clock::time_point  now_time;
          now_time = std::chrono::system_clock::now();
          double elapsed = (double)std::chrono::duration_cast<std::chrono::milliseconds>(now_time - image_time_prev_).count(); 
          image_time_prev_ = now_time;
          RCLCPP_INFO( this->get_logger(), "Cycle time %d %f", skip_count_, elapsed);
#endif

          rclcpp::Time timestamp = this->get_clock()->now();
          raw_to_mat(img_buffer_, &mat_images_);
          /// Publish raw image
          if (is_publish_raw_) {
            publish_raw(timestamp);
          }
          /// Publish pallarax image
          if (is_publish_pallarax_) {
            publish_pallarax(timestamp);
          }
          /// Publish demo images with pallarax color
          if (is_publish_demo_) {
            publish_demo(timestamp);
          }
          // Publish Point cloud
          if (is_publish_cloud2_) {
            publish_pcl(timestamp);
          }
        }
      }
    } else {
    /**
     * Publish stereo images
     */
      ret = ISCWrapper::get_stereo_images(img_buffer_, right_buffer_);
      if (ret == true) {
        skip_count_--;
        if( skip_count_ == 0 ) {
          skip_count_ = skip_count_init_;
#if (STOP_WATCH==1)
          std::chrono::system_clock::time_point  now_time;
          now_time = std::chrono::system_clock::now();
          double elapsed = (double)std::chrono::duration_cast<std::chrono::milliseconds>(now_time - image_time_prev_).count(); 
          image_time_prev_ = now_time;
          RCLCPP_INFO( this->get_logger(), "Cycle time %f", elapsed);
#endif
          rclcpp::Time timestamp = this->get_clock()->now();
          raw_to_mat(img_buffer_, &mat_images_);
          raw_to_mat(right_buffer_, &right_mat_images_);

          publish_stereo(timestamp);
        }
      }
    }
  }
}

void ISCDriver::publish_raw(rclcpp::Time timestamp)
{
  cv::Rect roi(0, 0, width_, height_);
  cv::Mat mat_image = mat_images_(roi);
  std_msgs::msg::Header header;
  img_bridge_ = cv_bridge::CvImage(header, sensor_msgs::image_encodings::MONO8, mat_image);
  img_msg_ = img_bridge_.toImageMsg();
  img_msg_->header.stamp = timestamp;
  img_msg_->header.frame_id = frame_id_;
  camera_info_pub_->publish(*img_msg_);
}

void ISCDriver::publish_pallarax(rclcpp::Time timestamp)
{
  cv::Mat mat_image(height_*3, width_, CV_8UC1, img_buffer_);
  float * sp = pallarax_buffer_;
  uint8_t * dp1;
  uint8_t * dp2;
  for (int y = 0; y < height_; y++) {
    dp1 = mat_image.ptr<uint8_t>(y + height_);
    dp2 = mat_image.ptr<uint8_t>(y + height_ + height_);
    for (int x = 0; x < width_; x++) {
      int i = (int)( (*sp++) * 256);
      *dp1++ = (i >> 8);
      *dp2++ = i;
    }
  }
  std_msgs::msg::Header header;
  pallarax_bridge_ = cv_bridge::CvImage(header, sensor_msgs::image_encodings::MONO8, mat_image);
  pallarax_msg_ = pallarax_bridge_.toImageMsg();
  pallarax_msg_->header.stamp = timestamp;
  pallarax_msg_->header.frame_id = frame_id_;
  pallarax_info_pub_->publish(*pallarax_msg_);
}

void ISCDriver::publish_demo(rclcpp::Time timestamp)
{
  uint8_t * sp = img_buffer_;
  float * fp = pallarax_buffer_;
  uint8_t * dp1;
  uint8_t * dp2;
  for (int y = 0; y < height_; y++) {
    dp1 = mat_demo_images_.ptr<uint8_t>(y);
    dp2 = mat_demo_images_.ptr<uint8_t>(y) + width_ * 3;
    for (int x = 0; x < width_; x++) {
      *dp1++ = *sp;
      *dp1++ = *sp;
      *dp1++ = *sp;
      sp++;

      int i = (int)(*fp++);
      *dp2++ = color_table_[i].R;
      *dp2++ = color_table_[i].G;
      *dp2++ = color_table_[i].B;
    }
  }

  std_msgs::msg::Header header;
  demo_bridge_ = cv_bridge::CvImage(header, sensor_msgs::image_encodings::RGB8, mat_demo_images_);
  demo_msg_ = demo_bridge_.toImageMsg();
  demo_msg_->header.stamp = timestamp;
  demo_msg_->header.frame_id = frame_id_;
  demo_info_pub_->publish(*demo_msg_);
}

void ISCDriver::publish_pcl(rclcpp::Time timestamp)
{
  struct CameraParam * sp = (struct CameraParam *)(img_buffer_);
  float d_inf = sp->d_inf;
  float bf = sp->bf;
  float base_length = sp->base_length;

  int idx = width_;
  pcl::PointCloud<pcl::PointXYZRGB> cloud_;
  cloud_.width = 1;
  cloud_.height = 0;
  for (int y = 1; y < height_; y++) {
    for (int x = 0; x < width_; x++) {
      uint8_t v = img_buffer_[idx];
      float d = pallarax_buffer_[idx++] - d_inf;
      if (d >= 1.0F) {
        pcl::PointXYZRGB pt;
        pt.r = v;
        pt.g = v;
        pt.b = v;
        pt.z = bf / d;
        pt.x = base_length / d * (x - center_x_);
        pt.y = base_length / d * (center_y_ - y);
        cloud_.points.push_back(pt);
        cloud_.height++;
      }
    }
  }

  if (cloud_.height > 0) {
    sensor_msgs::msg::PointCloud2 pcl_msg_;
    pcl::toROSMsg(cloud_, pcl_msg_);
    pcl_msg_.header.frame_id = frame_id_;
    pcl_msg_.header.stamp = timestamp;
    pcl_info_pub_->publish(pcl_msg_);
  }
}

void ISCDriver::publish_stereo(rclcpp::Time timestamp)
{
  cv::Rect roi(0, 0, width_, height_);
  {
    cv::Mat mat_image = mat_images_(roi);
    std_msgs::msg::Header header;
    img_bridge_ = cv_bridge::CvImage(header, sensor_msgs::image_encodings::MONO8, mat_image);
    img_msg_ = img_bridge_.toImageMsg();
    img_msg_->header.stamp = timestamp;
    img_msg_->header.frame_id = frame_id_;
    camera_info_pub_->publish(*img_msg_);
  }
  {
    cv::Mat right_image = right_mat_images_(roi);
    std_msgs::msg::Header header;
    right_img_bridge_ =
      cv_bridge::CvImage(header, sensor_msgs::image_encodings::MONO8, right_image);
    right_img_msg_ = right_img_bridge_.toImageMsg();
    right_img_msg_->header.stamp = timestamp;
    right_img_msg_->header.frame_id = frame_id_;
    stereo_right_info_pub_->publish(*right_img_msg_);
  }
}

void ISCDriver::raw_to_mat(uint8_t * src, cv::Mat * dest)
{
  uint8_t * sp = src;
  uint8_t * dp;
  for (int y = 0; y < height_; y++) {
    dp = dest->ptr<uint8_t>(y);
    memcpy(dp, sp, width_);
    sp += width_;
  }
}

void ISCDriver::make_color_table(struct _stRGB * color_table)
{
  short STEP_ = 12;
  int cnt = 0;
  int bnd1 = INVALID_DISPARITY + 1;
  int bnd2 = STEP_;
  int bnd3 = STEP_ * 2;
  int bnd4 = STEP_ * 3;
  int bnd5 = STEP_ * 4;
  int bnd6 = COLOR_PALETTE_SIZE / 2;

  for (; cnt < bnd1; cnt++) {
    color_table[cnt].B = 0;
    color_table[cnt].G = 0;
    color_table[cnt].R = 0;
  }
  for (; cnt < bnd2; cnt++) {
    color_table[cnt].B = 255;
    color_table[cnt].G = (unsigned char)(255 * ((float)(cnt - bnd1) / (float)(bnd2 - bnd1)));
    color_table[cnt].R = 0;
  }
  for (; cnt < bnd3; cnt++) {
    color_table[cnt].B =
      (unsigned char)(255 * (1.0 - ((float)(cnt - bnd2) / (float)(bnd3 - bnd2))));
    color_table[cnt].G = 255;
    color_table[cnt].R = 0;
  }
  for (; cnt < bnd4; cnt++) {
    color_table[cnt].B = 0;
    color_table[cnt].G = 255;
    color_table[cnt].R = (unsigned char)(255 * ((float)(cnt - bnd3) / (float)(bnd4 - bnd3)));
  }
  for (; cnt < bnd5; cnt++) {
    color_table[cnt].B = 0;
    color_table[cnt].G =
      (unsigned char)(255 * (1.0 - ((float)(cnt - bnd4) / (float)(bnd5 - bnd4))));
    color_table[cnt].R = 255;
  }
  for (; cnt < bnd6; cnt++) {
    color_table[cnt].B = (unsigned char)(255 * ((float)(cnt - bnd5) / (float)(bnd6 - bnd5)));
    color_table[cnt].G = 0;
    color_table[cnt].R = 255;
  }
  for (; cnt < COLOR_PALETTE_SIZE; cnt++) {
    color_table[cnt].B = 255;
    color_table[cnt].G = 255;
    color_table[cnt].R = 255;
  }
}


} // namespcace isc_camera
