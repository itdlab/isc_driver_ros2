//   Copyright 2019 ITDLab corporation
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

/**
 * @file  isc_wrapper.hpp
 * @brief Wrapper of ISC camera SDK
 * @date  2019/10/08
 * @author  Katsuichi Kigasawa
 */


#ifndef ISC_DRIVER_H
#define ISC_DRIVER_H

#include "rclcpp/rclcpp.hpp"
#include "ISCSDKLib.h"

#include <mutex>

#define MAX_WIDTH   4096
#define MAX_HEIGHT  2048
#define MAX_IMAGE_SIZE (MAX_WIDTH * MAX_HEIGHT)

namespace isc_camera
{

class ISCWrapper : ISCSDKLib
{
public:
  /// Camera parameter
  struct CameraParam
  {
    /// Magic code, fixed value=0x12345678
    uint magic;
    /// D INF, Fixed point value
    float d_inf;
    int d_inf_int;                  // d_inf *1000
    /// BF, Fixed point value
    float bf;
    int bf_int;                 // bf * 1000
    /// Base length, Fixed point value
    float base_length;
    int base_length_int;              // base_length *1000
  };

public:
  /**
   * @brief Constructor
   */
  ISCWrapper();

  /**
   * @brief Destructor
   */
  ~ISCWrapper();

  /**
   * @brief Open ISC camera
   *
   * @return  zero if success, else anyerror is ocuur
   *
   */
  int open();

  /**
   * @brief Close ISC camera
   */
  void close();

  /**
   * @brief Start Grab. Start to acquire images from ISC camera
   * @param [in] stereo If true Get left and right camera image mode
   * @return  true is success
   */
  bool start_grab(bool stereo);

  /**
   * @brief Stop Grab
   * @return  true is success
   */
  bool stop_grab();

  /**
   * @brief Get images(raw image and pallarax) from camera
   *
   * @param [in]  fps   Frame per second.(only 30 or 60)
   * @param [out] image New image
   * @param [out] depth New Parallax data
   *
   * @return  true if new image is available
   */
  bool get_images(uint8_t * image, float * depth);

  /**
   * @brief Get images(left and right) from camera
   *
   * @param [out] left New image
   * @param [out] right New image
   *
   * @return  true if new image is available
   */
  bool get_stereo_images(uint8_t * left_image, uint8_t * right_image);

  /**
   * @brief Get size of valid image
   *
   * @param [out] width     image width
   * @param [out] height  image height
   * @return  true if successs
   */
  bool get_size(int * width, int * height);

  /**
   * @brief get camera informatio like width, height, bf,,,)
   *
   * @param [out] info      camera information
   *
   * @return  true if successs
   */
  bool get_camera_info(struct ISCSDKLib::CameraParamInfo * info);

  /**
   * @brief Set Shutter control
   *
   * @param [in] is_auto  Auto shutter control
   * @param [in] gain Gain value
   * @param [in] exposure Exposure value
   * @return  true if successs
   */
  bool set_shutter_control(bool is_auto, int gain, int exposure);

private:
  /// mutex for API
  std::mutex lock_;

  /// true if already opened
  bool is_open_;
  /// true is aleady start grab
  bool is_start_;
  /// model =0:VM, =1:XC
  int model_;
  /// Shutter control =1:camera auto control
  int shutter_auto_;
  /// Gain value in manual shutter control
  int shutter_gain_;
  /// Exposure value in manual shutter control
  int shutter_exposure_;
  /// Camera information
  struct CameraParamInfo info_;
  /// Image buffer
  uint8_t * image_;
  /// Pallarax data buffer
  uint8_t * pallarax_;
  /// Depth data buffer
  float * depth_;

  /// Camera paameter to convert pallarax to XYZ
  struct CameraParam camera_param_;
};

} // namespace isc_camera

#endif
