//   Copyright 2019 ITDLab corporation
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
/// @file   isc_driver.hpp
/// @brief  There are defines for ISC camera driver
/// @date   2019/10/08
/// @author Katsuichi Kigasawa


#ifndef ISC_DRIVER_NODE_HPP_
#define ISC_DRIVER_NODE_HPP_

#include <chrono>
#include <iostream>
#include <stdio.h>
#include <memory>
#include <string>
#include <thread>
#include <utility>

#include "rclcpp/rclcpp.hpp"
#include "rclcpp/publisher.hpp"
#include "rclcpp_lifecycle/lifecycle_node.hpp"
#include "rclcpp_lifecycle/lifecycle_publisher.hpp"

#include <sensor_msgs/msg/point_cloud2.hpp>
#include "sensor_msgs/msg/image.hpp"
#include "sensor_msgs/image_encodings.hpp"
#include <pcl_conversions/pcl_conversions.h>

#include "opencv2/highgui/highgui.hpp"
#include "cv_bridge/cv_bridge.h"

#include "isc_driver/isc_wrapper.hpp"

using namespace std::chrono_literals;


#define INVALID_DISPARITY       3

namespace isc_camera
{

class ISCDriver : public rclcpp_lifecycle::LifecycleNode, ISCWrapper
{
public:
  /**
   * @brief  Constructer
   */
  ISCDriver(const std::string model, const std::string & node_name, bool intra_process_comms = false);
  /**
   * @brief  Destructor
   */
  ~ISCDriver();

  /**
   * @brief Update parameters callback
   */
  rcl_interfaces::msg::SetParametersResult param_update_callback(std::vector<rclcpp::Parameter> params);

  /**
   * @brief Lifecycle : Configure
   * @return  SUCCESS if success
   */
  rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn on_configure(
    const rclcpp_lifecycle::State &);
  /**
   * @brief Lifecycle : Activate
   * @return  SUCCESS if success
   */
  rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn on_activate(
    const rclcpp_lifecycle::State &);
  /**
   * @brief Lifecycle : DeActivate
   * @return  SUCCESS if success
   */
  rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn on_deactivate(
    const rclcpp_lifecycle::State &);
  /**
   * @brief Lifecycle : Cleanup
   * @return  SUCCESS if success
   */
  rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn on_cleanup(
    const rclcpp_lifecycle::State &);
  /**
   * @brief Lifecycle : Shutdown
   * @return  SUCCESS if success
   */
  rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn on_shutdown(
    const rclcpp_lifecycle::State &);

private:
  /**
   * Color table to convert pallarax to color image
   */
  struct _stRGB
  {
    uint8_t R;
    uint8_t G;
    uint8_t B;
  };
  /**
    * @brief Start Grab.
    *        Start to acquire images from ISC camera
    * @return  true if success
    */
  bool start_grab();
  /**
   * @brief Stop Grab.
   *
   */
  void stop_grab();
  /**
   * @brief Convert raw image to Mat
   *
   * @param [in]  src   Raw image as source
   * @param [out] dest  image as Mat
   */
  void raw_to_mat(uint8_t * src, cv::Mat * dest);
  /**
   * @brief Cyclic timer
   *        process to acquire image from camera
   *        publish image, pallarax and demo image
   *
   */
  void image_callback();

  /**
   * @brief Publish Raw image
   */
  void publish_raw(rclcpp::Time timestamp);
  /**
   * @brief Publish Raw and pallarax image
   */
  void publish_pallarax(rclcpp::Time timestamp);
  /**
   * @brief Publish Raw and colored image by depth
   */
  void publish_demo(rclcpp::Time timestamp);
  /**
   * @brief Publish point cloud
   */
  void publish_pcl(rclcpp::Time timestamp);
  /**
   * @brief Publish stereo images
   */
  void publish_stereo(rclcpp::Time timestamp);


  /**
   * @brief Make color table of pallarax to color image
   *
   * @param [out] color_table  Color table
   */
  void make_color_table(struct _stRGB * color_table);

  /// Cyclic timer of 10msec
  rclcpp::TimerBase::SharedPtr timer_;

  /// Model(VM or XC)
  std::string model_;
  /// Frame ID
  std::string frame_id_;
  /// Frame per sec. only 30 or 60
  int fps_;
  /// If true Publish stereo image
  int is_publish_stereo_;
  /// If true Publish raw image
  int is_publish_raw_;
  /// If true Publish raw image with pallarax
  bool is_publish_pallarax_;
  /// If true Publish point cloud2
  bool is_publish_cloud2_;
  /// If true Publish demo
  bool is_publish_demo_;
  /// Auto shutter control, auto if true
  bool is_shutter_auto_;
  /// Gain value in manual shuuter mode
  int shutter_gain_;
  /// Exposure value in manual shuuter mode
  int shutter_exposure_;
  /// Image height like 480 or 720
  int height_;
  /// Image width like 640 or 1024
  int width_;
  /// Center X position of image
  int center_x_;
  /// Center Y position of image
  int center_y_;
  /// Raw image buffer from camera
  uint8_t * img_buffer_;
  /// Right image buffer from camera
  uint8_t * right_buffer_;
  /// pallarax buffer from camera
  float * pallarax_buffer_;

  /// Publisher of Raw image
  std::shared_ptr<rclcpp_lifecycle::LifecyclePublisher<sensor_msgs::msg::Image>> camera_info_pub_;
  /// Message of Raw image
  std::shared_ptr<sensor_msgs::msg::Image> img_msg_;
  /// CV Bridge of Raw image
  cv_bridge::CvImage img_bridge_;

  /// Publisher of Right image
  std::shared_ptr<rclcpp_lifecycle::LifecyclePublisher<sensor_msgs::msg::Image>>
  stereo_right_info_pub_;
  /// Message of RIght image
  std::shared_ptr<sensor_msgs::msg::Image> right_img_msg_;
  /// CV Bridge of Right image
  cv_bridge::CvImage right_img_bridge_;

  /// Publisher of Pallarax image
  std::shared_ptr<rclcpp_lifecycle::LifecyclePublisher<sensor_msgs::msg::Image>> pallarax_info_pub_;
  /// Message of Pallarax image
  std::shared_ptr<sensor_msgs::msg::Image> pallarax_msg_;
  /// CV Bridge of Pallarax image
  cv_bridge::CvImage pallarax_bridge_;

  /// CV Bridge of Demo image
  cv_bridge::CvImage demo_bridge_;
  /// Publisher of Demo image
  std::shared_ptr<rclcpp_lifecycle::LifecyclePublisher<sensor_msgs::msg::Image>> demo_info_pub_;
  /// Message of Demo image
  std::shared_ptr<sensor_msgs::msg::Image> demo_msg_;
  /// Demo image
  cv::Mat mat_demo_images_;

  /// Publisher of Point cloud2
  std::shared_ptr<rclcpp_lifecycle::LifecyclePublisher<sensor_msgs::msg::PointCloud2>> pcl_info_pub_;
  /// Message of Point cloud2
  std::shared_ptr<sensor_msgs::msg::PointCloud2> pcl_msg_;

  /// Get image timing
  std::chrono::system_clock::time_point  image_time_prev_;
  /// Initial value of Image Skip counter 
  int skip_count_init_;
  /// Image Skip counter 
  int skip_count_;

private:
  /// true if start GRAB, acquiring image from camera
  bool is_start_;
  /**
   * Mat image of Raw image and Pallarax.
   * Height is actual image height times 3, width is actual image width.
   * Include three separated blocks. One block is image of height x width.
   * First block is the raw image.
   * Second block is the integral part of Pallarax.
   * Third block is the decimal part of Pallarax.
   */
  cv::Mat mat_images_;
  /// Mat image of Right Raw image.
  cv::Mat right_mat_images_;

  /// Color table to convert pallarax to color image
  struct _stRGB color_table_[256];

};
} // isc_camera
#endif // ISC_CAMERA_DRIVER_HPP_
