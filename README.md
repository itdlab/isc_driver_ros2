# ROS2 ISC camera driver LIFECYCLE Node
=======================================

    This is the LIFECYCLE NODE to pubish some kind of images from the ITD Lab's ISC(Intelligent stereo camera).
    This driver supports XC model and VM model.
    XC model is 1024x720.
    VM model is 640x480.

# Topics
    '/isc_camera/image_raw'       - Calibrated image of left sensor (*sensor_msgs::msg::Image*)
    '/isc_camera/pallarax'        - Calibrated image and pallarax data (*sensor_msgs::msg::Image*)
    '/isc_camera/left/image_raw'  - Calibrated image of left sensor (*sensor_msgs::msg::Image*)
    '/isc_camera/right/image_raw' - Calibrated image of right sensor (*sensor_msgs::msg::Image*)
    '/isc_camera/demo'            -  Raw image and colored pallarax data (*sensor_msgs::msg::Image*)
    '/isc_camera/point_cloud'     - Point cloud as left hand coordinate (*sensor_msgs::msg::PointCloud2*)

    Note - Image includes Camera parameters(struct CameraParam) in the top-left of the image.
            You can Convert it with pallarax data and CameraParam to XYZ.
    Note - The format of /isc_camera/pallarax is below.
    +------------------------+
    | raw image              |
    | 8bit per pixel         |
    | 1024 x 720 size        |
    +------------------------+
    | pallarax integer part  |           15           8 7           0 bit 
    | 8bit per each          |           +-------------+-------------+
    | 1024 x 720             | --------->|integer part |decimal part |
    +------------------------+           +-------------+-------------+
    | pallarax decimal part  |                                ^
    | 8bit per each          |                                |
    | 1024 x 720             | -------------------------------+
    +------------------------+
    VM model is 640x480 instead of 1024x720.

# Installation
## Installation for XC model
	sudo apt-get install usb-1.0
	sudo apt-get install libconfuse-dev
	sudo dpkg -i lib/iscsdk2_0.9.2-2_amd64.deb

    Please download FTDI D3xx driver from the below and install.
    https://www.ftdichip.com/Drivers/D3XX.htm

## Installation for VM model
	sudo apt-get install usb-1.0
	sudo apt-get install libconfuse-dev
	git clone git://developer.intra2net.com/libftdi
	mkdir -p libftdi/build
	cd libftdi/build
	sudo cmake –DCMAKE_INSTALL_PREFIX=”/usr/local/src/ftdi” ../
	sudo make
	sudo make install

	sudo dpkg -i lib/iscsdk1_0.0.9-3_amd64.deb

# Build
    colcon build
    Please change ISC_MODEL to 2 in CMakeLists.txt if XC model . Change ISC_MODEL to 1 if VM model.

# Run
## Run as Superuser
    sudo su
    source /opt/ros/dashing/setup.bash
    source install/setup.bash

## Run XC model
    ros2 run isc_driver xc_driver_node
    or
    ros2 launch isc_driver xc_driver_launch.py

    ros2 lifecycle set xc_driver_node configure
    ros2 lifecycle set xc_driver_node activate
    ros2 lifecycle set xc_driver_node deactivate
    ros2 lifecycle set xc_driver_node shutdown

## Run VM model
    ros2 run isc_driver vm_driver_node
    or
    ros2 launch isc_driver vm_driver_launch.py

    ros2 lifecycle set vm_driver_node configure
    ros2 lifecycle set vm_driver_node activate
    ros2 lifecycle set vm_driver_node deactivate
    ros2 lifecycle set vm_driver_node shutdown

# Parameters
    frame_id: (string default:isc_camera) - Frame id
    fps:  (integer default:30) - Frame per sec(30 or 60)
    publish:
      stereo: (bool default:0)   - publish only stereo image to /isc_camera/left and /isc_camera/right. raw, pallarax, cloud2 and demo DO NOT publish if true.
      raw: (bool default:true) - publish raw image to /isc_camera/image_raw
      pallarax: (bool default:false) - publish raw image and pallarax to /isc_camera/pallarax
      cloud2: (bool default:true) - publish point cloud2 to /isc_camera/point_cloud
      demo: (bool default:false) - publish raw image and colored pallarax to /isc_camera/demo
    shutter:
      auto: (bool default:yes) - auto shutter control
      gain: (integer default:16) - Gain value if shuttter control is not auto
                                 - The value is from 16 to  64 as VM model
                                 - The value is from  0 to 720 as XC model
      exposure: (integer default:479) - exposure value if shuttter control is not auto
                                 - The value is from  1 to 479 as VM model
                                 - The value is from  2 to 748 as XC model(2 is brightest,  748 is darkest)
## Change parameter auto, gain and exposure in active
    ros2 param set vm_driver_node shutter.auto false
    ros2 param set vm_driver_node shutter.gain 16
    ros2 param set vm_driver_node shutter.exposure 480
    ros2 param set xc_driver_node shutter.auto false
    ros2 param set xc_driver_node shutter.gain 16
    ros2 param set xc_driver_node shutter.exposure 480


